#  MEAN3 Scoreboard

## Usage
To turn on the scoreboard:
  1. Open a command prompt or terminal
  2. Navigate to the scoreboard folder containing the webhost.js file
  3. Execute the following command:
    ```
    node webhost.js
    ```
Open a browser of your choice and navigate to localhost:#####/ to open the scoreboard page

To add a team to the competition, click the Add Team entry on the home page.  This will open the Add Team modal.

To edit a team, click on the given team entry.  This will open the Team Profile modal.

To start a run with a team, click the Start Run button on the given team entry.  This will open the Competition Modal.



## Configuration
Team rank will default to 0 when the team is added, placing teams in FIFO order on the table above any teams that have competed so far.  Once a team has competed and their score computed, their entry will rank beneath teams that have yet to compete in the position corresponding to their current rank.

To change this behavior, go to the Settings modal.
