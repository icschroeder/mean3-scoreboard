(function(angular) {
  'use strict';

  angular.module('pro-mod',[])

  .controller('pro-ctrl',['$scope','$http','$window',function($scope,$http,$window) {
    $scope.update = function() {
      const url = '/update_team?select='+$scope.backup.name+'&nm='+$scope.team.name+'&sch='+$scope.team.school+'&run1='+$scope.team.run1+'&pen1='+$scope.team.pen1+'&run2='+$scope.team.run2+'&pen2='+$scope.team.pen2+'&rnk='+$scope.team.rank;
      console.log(url);
      $http.get(url);
      // $window.location = '/';
    };
    $scope.destroy = function() {
      const url = '/destroy_team?nm='+$scope.backup.name;
      console.log(url);
      $http.get(url);
      $window.location = '/';
    };
    // $scope.destroy_all = function() {
    //     console.log("destory_all");
    // };
    // $scope.reset = function() {
    //   $scope.team = {};
    // };
    $scope.back1 = function() {
      $scope.$parent.mod_state_pro = '';
      $scope.$parent.mod_state_fade = '';
    };
  }]);
})(angular);
