(function(angular) {
  'use strict';

  angular.module('set-mod',[])

  .controller('set-ctrl',['$scope',function($scope) {
    $scope.tmp_round_timer = $scope.round_length;
    $scope.set_round_timer = function(i) {
      $scope.round_length = i;
      $scope.round_timer = $scope.round_length;
    };
    $scope.update2 = function() {
      $scope.set_round_timer($scope.tmp_round_timer);
    };
    $scope.back = function() {
      $scope.$parent.mod_state_set = '';
      $scope.$parent.mod_state_fade = '';
    };

  }]);
})(angular);
