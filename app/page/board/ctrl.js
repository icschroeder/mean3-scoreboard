(function(angular) {
  'use strict';

  angular.module('board-mod',[])

  .factory('appService', ['$http',function($http) {
    return {
      getTeams: function() {
        return $http.get('/data/team.json').then(function(result) {
          return result.data;
        });
      },
      getGameState: function() {
        return $http.get('/data/game.json').then(function(result) {
          return result.data;
        });
      }
    }
  }])

  .controller('board-ctrl',['$scope','appService',function($scope,appService) {
    $scope.selected;
    $scope.round_length = 10; // # of seconds, 10 minutes
    $scope.round_timer = $scope.round_length;
    $scope.teams = [];
    $scope.rounds = [];
    $scope.team = {
      'name':'',
      'school':'',
      'time':0,
      'score':0,
      'rank':0
    };
    appService.getTeams().then(function(teams) {
      $scope.teams = teams;
    });
    appService.getGameState().then(function(rounds) {
      $scope.rounds = rounds;
    });
    $scope.set_selected = function(i) {
      $scope.selected = $scope.teams[i].name;
      $scope.team.name = $scope.teams[i].name;
      $scope.team.school = $scope.teams[i].school;
      $scope.team.time = $scope.teams[i].time;
      $scope.team.score = $scope.teams[i].score;
      $scope.team.rank = $scope.teams[i].rank;
    };

    $scope.show_add = function() {
      $scope.$parent.mod_state_add = 'modal_show';
      $scope.$parent.mod_state_fade = 'modal_show';
    };
    $scope.show_team = function(i) {
      $scope.set_selected(i);
      $scope.$parent.mod_state_pro = 'modal_show';
      $scope.$parent.mod_state_fade = 'modal_show';
    };
    $scope.show_settings = function() {
      $scope.$parent.mod_state_set = 'modal_show';
      $scope.$parent.mod_state_fade = 'modal_show';
    };
    $scope.show_run = function(i) {
      $scope.set_selected(i);
      $scope.$parent.mod_state_run = 'modal_show';
      $scope.$parent.mod_state_fade = 'modal_show';
    };
  }]);
})(angular);
