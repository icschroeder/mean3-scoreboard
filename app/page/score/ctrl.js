(function(angular) {
  'use strict';

  angular.module('score-mod',[])

  .controller('score-ctrl',['$scope','$window','$http',function($scope,$window,$http) {
    $scope.back2 = function() {
      $scope.$parent.mod_state_add = '';
      $scope.$parent.mod_state_fade = '';
    };

    $scope.create = function() {
      var url = 'http://localhost:28374/create_team?nm=' + $scope.new_team.name + '&sch=' + $scope.new_team.school;
      $http.get(url);
      $scope.$parent.mod_state_add = '';
      $scope.$parent.mod_state_fade = '';
      $window.location = '/';
    };
  }]);
})(angular);
