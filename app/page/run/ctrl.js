(function(angular) {
  'use strict';

  angular.module('round-mod',[])

  .controller('round-ctrl',['$scope','$window','$interval',function($scope,$window,$interval) {
    $scope.round_back = function() {
      $scope.$parent.mod_state_run = '';
      $scope.$parent.mod_state_fade = '';
    };

    // local persistence, game record
    $scope.round = [];

    // countdown
    $scope.round_interval;
    $scope.round_state = true;
    // $scope.round_timer is in app module
    $scope.round_num = 0;

    // countdown functions
    $scope.round_start = function() {
      $scope.round_reset();
      $scope.round_interval = $interval(function() {
        if ($scope.round_timer > 0) {
          $scope.round_timer--;
          $scope.round_timer_min = $scope.round_timer/60;
          $scope.round_timer_sec = $scope.round_timer%60;
        } else {
          $scope.round_stop();
        }
      },1000);
    };
    $scope.round_toggle_pause = function() {
      if ($scope.round_state) {
        $scope.round_state = false;
      } else {
        $scope.round_state = true;
      }
    };
    $scope.round_reset = function() {
      $scope.round_timer = $scope.round_length;
      $scope.round_stop();
    };
    $scope.round_stop = function() {
      $interval.cancel($scope.round_interval);
      $scope.round_interval = undefined;
      $scope.round_end();
    };
    $scope.round_end = function() {
      // close round modal, open scoring modal
      console.log($scope.round);

      // compute best time -> best score & assign to team
      var best_time;
      var best_score;
      console.log('best time: ' + best_time);
      console.log('best score: ' + best_score);

      // save all laps to file
      // $http.get('/create_run?');
    };

    // timer
    $scope.run_interval;
    $scope.run_state = true;
    $scope.run_timer = 0;
    $scope.run_num = 0;

    // timer functions
    $scope.run_start = function() {
      $scope.run_timer = 0;
      $scope.run_state = true;
      $scope.run_interval = $interval(function() {
        if ($scope.run_state) {
          $scope.run_timer++;
          $scope.run_timer_min = $scope.run_timer/60;
          $scope.run_timer_sec = $scope.run_timer%60;
        }
      },1000)
    };
    $scope.run_toggle_pause = function() {
      if ($scope.run_state) {
        $scope.run_state = false;
      } else {
        $scope.run_state = true;
      }
    };
    $scope.run_reset = function() {
      $scope.run_stop();
      $scope.run_timer = 0;
    };
    $scope.run_stop = function() {
      $interval.cancel($scope.run_interval);
      $scope.run_interval = undefined;
      $scope.run_end();
    };
    $scope.run_end = function() {
      $scope.round.push({
        'id': $scope.run_num++,
        'team': $scope.team.name,
        'round': $scope.round_num,
        'time': $scope.run_timer,
        'penalty': 0,
        'score': $scope.run_timer
      });
    };

  }]);
})(angular);
