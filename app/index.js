(function(angular) {
  'use strict';

  angular.module('app-mod', [
  'board-mod',
  'round-mod',
  'pro-mod',
  'add-mod',
  'set-mod'])

  .controller('app-ctrl', ['$scope',function($scope) {
    $scope.app_back = function() {
      $scope.$parent.mod_state_add = '';
      $scope.$parent.mod_state_pro = '';
      $scope.$parent.mod_state_run = '';
      $scope.$parent.mod_state_set = '';
      $scope.$parent.mod_state_fade = '';
    }
  }])

  .directive('scoreboard',[function() {
    return {
      restrict: 'EA',
      replace: false,
      templateUrl: 'page/board/view.html',
      controller: 'board-ctrl'
    }
  }])

  .directive('runModal',[function() {
    return {
      restrict: 'EA',
      replace: false,
      templateUrl: 'page/run/view.html',
      controller: 'round-ctrl'
    }
  }])

  .directive('addModal',[function() {
    return {
      restrict: 'EA',
      replace: false,
      templateUrl: 'page/add/view.html',
      controller: 'add-ctrl'
    }
  }])

  .directive('proModal',[function() {
    return {
      restrict: 'EA',
      replace: false,
      templateUrl: 'page/profile/view.html',
      controller: 'pro-ctrl'
    }
  }])

  .directive('setModal',[function() {
    return {
      restrict: 'EA',
      replace: false,
      templateUrl: 'page/settings/view.html',
      controller: 'set-ctrl'
    }
  }])

  .directive('scoreModal',[function() {
    return {
      restrict: 'EA',
      replace: false,
      tempalteUrl: 'page/score/view.html',
      controller: 'score-ctrl'
    }
  }])

  .directive('header',[function() {
    return {
      restrict: 'EA',
      replace: false,
      templateUrl: 'page/frame/header.html'
    }
  }])

  .directive('footer',[function() {
    return {
      restrict: 'EA',
      replace: false,
      templateUrl: 'page/frame/footer.html'
    }
  }]);
})(angular);
