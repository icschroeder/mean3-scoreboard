// add security
const fs = require('fs');
const express = require('express');
const app = express();
const team_file = '/app/data/team.json';
const game_file = '/app/data/game.json';

app.use(express.static(__dirname + '/app'));

// local persistence
app.get('/create_team',(req,res) => {
  var out = {
    'name': req.query.nm,
    'school': req.query.sch,
    'time': 0,
    'score': 0,
    'rank': 0
  };
  fs.readFile(__dirname+team_file,(err,data) => {
    var exists = false;
    if (err) throw err;
    var json = JSON.parse(data);
    for (var i = 0; i < json.length; i++) {
      if (json[i].name == req.query.select) {
        exists = true;
      }
    }
    if (!exists) {
      json.push(out);
    }
    fs.writeFile(__dirname+team_file,JSON.stringify(json));
  });
  res.sendStatus(200);
});
app.get('/read_teams',(req,res) => {
  fs.readFile(__dirname+team_file,(err,data) => {
    if (err) throw err;
    var json = JSON.parse(data);
    res.send(json);
  });
});
app.get('/update_team',(req,res) => {
  fs.readFile(__dirname+team_file,(err,data) => {
    if (err) throw err;
    var json = JSON.parse(data);
    for (var i = 0; i < json.length; i++) {
      if (json[i].name == req.query.select) {
        json[i].name = req.query.nm || json[i].name;
        json[i].school = req.query.sch || json[i].school;
        json[i].time = req.query.tm || json[i].time;
        json[i].score = req.query.scr || json[i].score;
        json[i].rank = req.query.rnk || json[i].rank;
      }
    }
    fs.writeFile(__dirname+team_file,JSON.stringify(json));
  });
  res.sendStatus(200);
});
app.get('/destroy_team',(req,res) => {
  fs.readFile(__dirname+team_file,(err,data) => {
    if (err) throw err;
    var json = JSON.parse(data);
    for (var i = 0; i < json.length; i++) {
        if (json[i].name == req.query.nm) {
          json.splice(i,1);
        }
    }
    fs.writeFile(__dirname+team_file,JSON.stringify(json));
  });
});
app.get('/destroy_teams',(req,res) => {
  fs.writeFile(__dirname+team_file,'[]');
  res.sendStatus(200);
});
app.get('/create_run',(req,res) => {
  var out = {
    'id': 0,
    'team': '',
    'round': 0,
    'time': 0,
    'penalty': 0,
    'score': 0
  };
  fs.readFile(__dirname+team_file,(err,data) => {
    var exists = false;
    if (err) throw err;
    var json = JSON.parse(data);
    json.push(out);
    fs.writeFile(__dirname+team_file,JSON.stringify(json));
  });
  res.sendStatus(200);
});
app.get('/delete_run',(req,res) => {

});

// add routes for storing data to database
// add routes for exporting

app.listen(28374)
console.log('live on 28374')
